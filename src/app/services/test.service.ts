import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { TestInterface } from '../models/test.interface';



@Injectable({
    providedIn: 'root'
})
export class TestService {

    private endPoint: string = 'https://randomapi.com/api/e2mikld3?key=6IF9-3QF1-IJMV-OM2E&results=5&fmt=raw';
    private headers: any = '';

    httpOptions = {
        headers: new HttpHeaders(this.headers),
    };

    constructor(private http: HttpClient, private router: Router) { }

    getTest(): Observable<TestInterface[]> {//key=ABCD-1234-EFGH-5678&ref=1234abcd
        return this.http.get<TestInterface[]>(`${this.endPoint}`, this.httpOptions);
    }
    
}
