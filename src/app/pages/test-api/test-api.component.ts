import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { TestInterface } from 'src/app/models/test.interface';
import { TestService } from 'src/app/services/test.service';
import { DialogInsertComponent } from './dialog-insert/dialog-insert.component';

export interface DialogData {
  name: string;
  age: string;
  sex: string;
}

@Component({
  selector: 'app-test-api',
  templateUrl: './test-api.component.html',
  styleUrls: ['./test-api.component.scss']
})
export class TestApiComponent implements OnInit {
  displayedColumns: string[] = ['name', 'age', 'sex', 'documento'];
  dataSource: any;
  datos = [];
  tastInterface!: TestInterface[];
  constructor(
    public dialog: MatDialog,
    private testService: TestService,
    ) { 

    }

  ngOnInit(): void {
    this.getTest();
  }

  getTest(){
    this.testService.getTest().subscribe(
      resp => {
        console.log(this.tastInterface = resp);
        this.dataSource = new MatTableDataSource(this.tastInterface);
      }
    );
  }

  openDialog() {
    this.dialog.open(DialogInsertComponent, {
      data: {
      },
    })
  };

}
