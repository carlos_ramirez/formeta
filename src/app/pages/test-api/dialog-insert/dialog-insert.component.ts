import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../test-api.component';

@Component({
  selector: 'app-dialog-insert',
  templateUrl: './dialog-insert.component.html',
  styleUrls: ['./dialog-insert.component.scss']
})
export class DialogInsertComponent implements OnInit {

  form: FormGroup;

  get name(){
    return localStorage.getItem('Nombre') ?? '';
  }

  get age() {
    return localStorage.getItem('Edad') ?? '';
  }

  get sex() {
    return localStorage.getItem('Genero') ?? '';
  }

  get documento() {
    return localStorage.getItem('Documento') ?? '';
  }
  
  constructor(
    public dialogRef: MatDialogRef<DialogInsertComponent>,
    @Inject(MAT_DIALOG_DATA) 
    public data: DialogData,
    private fb: FormBuilder,

    ) {

    this.form = this.fb.group({
      name: ['', [Validators.required]],
      age: ['', [Validators.required]],
      sex: ['', [Validators.required]],
      documento: ['', [Validators.required]],
    });
   }

  ngOnInit(): void {
  }

  create() {
    const val = this.form.value;
    if (val.name && val.age && val.sex) {
      localStorage.setItem("Nombre",val.name);
      localStorage.setItem("Edad",val.age);
      localStorage.setItem("Genero",val.sex);
      localStorage.setItem("Documento",val.documento);
    }
    return null;
  }

  clear(){
    localStorage.removeItem("Nombre");
    localStorage.removeItem("Edad");
    localStorage.removeItem("Genero");
    localStorage.removeItem("Documento");
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
